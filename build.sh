#!/bin/sh

PACKAGES="$(find . -name 'APKBUILD' -exec dirname {} \;)"

for package in $PACKAGES; do
  echo "Building package $package"
  cd $package
  abuild checksum
  abuild -r
done

